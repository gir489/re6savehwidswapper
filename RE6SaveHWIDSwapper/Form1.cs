﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using SharpMik.Player;
using SharpMik.Drivers;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics;

namespace RE6SaveHWIDSwapper
{
    public partial class MainForm : Form
    {
        byte[] targetFile;
        private const int HWID_FILE_OFFSET = 0x10;
        String saveFileLocation;
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var exists = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)).Length > 1;
            if (exists)
            {
                MessageBox.Show("An instance of RE6 Save Transfer By gir489 is already running.");
                this.Close();
            }
            MikMod m_Player = new MikMod();
            m_Player.Init<NaudioDriver>("");
            Assembly _assembly = Assembly.GetExecutingAssembly();
            SharpMik.Module m_Mod = m_Player.LoadModule(_assembly.GetManifestResourceStream("RE6SaveHWIDSwapper.FFF - Half Life 2 Cheats Enabler.xm"));
            m_Mod.wrap = true;
            m_Player.Play(m_Mod);
        }

        private void btnFrom_Click(object sender, EventArgs e)
        {
            OpenFileDialog fromFileDialog = new OpenFileDialog();
            String dirSteam = (String)Registry.GetValue("HKEY_CURRENT_USER\\Software\\Valve\\Steam", "SteamPath", null);
            if (String.IsNullOrEmpty(dirSteam) == false)
            {
                dirSteam = dirSteam.Replace("/", "\\") + "\\userdata";

                string[] fileEntries = Directory.GetDirectories(dirSteam);
                foreach (String userId in fileEntries)
                {
                    if (Directory.Exists(userId + "\\221040"))
                    {
                        fromFileDialog.InitialDirectory = userId + "\\221040\\remote";
                    }
                }
            }
            if (String.IsNullOrEmpty(fromFileDialog.InitialDirectory))
            {
                String dirRELOADED = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Steam\\!RLD\\221040\\storage";
                String dir3DM = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Steam\\3DMGAME\\221040\\storage";
                if (File.Exists(dirRELOADED + "\\savedata.bin"))
                {
                    fromFileDialog.InitialDirectory = dirRELOADED;
                }
                else if (File.Exists(dir3DM + "\\savedata.bin"))
                {
                    fromFileDialog.InitialDirectory = dir3DM;
                }
                else
                {
                    fromFileDialog.InitialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"; //This PC GUID
                    MessageBox.Show("Failed to detect Steam/RELOADED/3DMGAME save file location. Please select save file location manually.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            fromFileDialog.Filter = "RE6 Save File|savedata.bin";
            fromFileDialog.RestoreDirectory = true;
            if (fromFileDialog.ShowDialog() == DialogResult.OK)
            {
                saveFileLocation = fromFileDialog.FileName.Replace("\\savedata.bin", "");
                try
                {
                    using (BinaryReader reader = new BinaryReader(fromFileDialog.OpenFile()))
                    {
                        byte[] HWID = new byte[8];
                        reader.BaseStream.Seek(HWID_FILE_OFFSET, SeekOrigin.Begin);
                        reader.Read(HWID, 0, 8);
                        String HWIDAsString = BitConverter.ToString(HWID).Replace("-", "");
                        txtHWID.Text = HWIDAsString;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to read original save file from disk. C# exception: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                saveFileLocation = null;
            }
        }
        
        private void txtHWID_TextChanged(object sender, EventArgs e)
        {
            txtHWID.Text = Regex.Replace(txtHWID.Text, "[^0-9A-Fa-f]", "").ToUpper();
            if (!ModifierKeys.HasFlag(Keys.Control))
                txtHWID.SelectionStart = txtHWID.Text.Length;
        }

        private void btnSelectTargetSave_Click(object sender, EventArgs e)
        {
            OpenFileDialog targetFileDialog = new OpenFileDialog();
            targetFileDialog.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            targetFileDialog.Filter = "RE6 Save File|savedata.bin";
            targetFileDialog.RestoreDirectory = true;
            if (targetFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    targetFile = File.ReadAllBytes(targetFileDialog.FileName);
                    if (targetFile.Length != 39416)
                    {
                        MessageBox.Show("Incorrect file size detected.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        targetFile = null;
                        txtStatus.Text = "Need Target File";
                        txtStatus.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        txtStatus.Text = "Target File Loaded";
                        txtStatus.ForeColor = System.Drawing.Color.Green;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to read target savefile from disk. C# exception: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(saveFileLocation))
            {
                MessageBox.Show("Please select the original file directory by using the 'Get HWID From Original' button before using this feature.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (txtHWID.Text.Length != 16)
            {
                MessageBox.Show(txtHWID.Text + " is not a valid HWID.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (targetFile == null || targetFile.Length != 39416)
            {
                MessageBox.Show("You must load a proper target file first.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                File.Copy(saveFileLocation + "\\savedata.bin", saveFileLocation + "\\savedata.backup." + DateTime.Now.ToString("MM.dd.yyyy.HH.mm.ss") + ".bin");
            }
            catch(Exception ex)
            {
                MessageBox.Show("Failed to backup original savefile. C# exception: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            for(int i = 0; i < 8; i++)
            {
                byte outByte;
                Byte.TryParse(txtHWID.Text.Substring(i*2, 2), NumberStyles.HexNumber, null, out outByte);
                targetFile[HWID_FILE_OFFSET + i] = outByte;
            }
            try
            {
                File.WriteAllBytes(saveFileLocation + "\\savedata.bin", targetFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to write savefile. C# exception: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("Wrote target savedata.bin with HWID " + txtHWID.Text + " to " + saveFileLocation + "\\savedata.bin");
        }

        static int SearchBytes(byte[] haystack, byte[] needle)
        {
            var len = needle.Length;
            var limit = haystack.Length - len;
            for (var i = 0; i <= limit; i++)
            {
                var k = 0;
                for (; k < len; k++)
                {
                    if (needle[k] != haystack[i + k]) break;
                }
                if (k == len) return i;
            }
            return -1;
        }

        private void patchBH6exeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String exeLocation = null;
            Process[] re6EXE = Process.GetProcessesByName("BH6");
            if (re6EXE.Length == 1)
            {
                exeLocation = re6EXE[0].MainModule.FileName;
                MessageBox.Show("Detected RE6 running. Close RE6, then click OK.");
            }
            else
            {
                String dirSteam = (String)Registry.GetValue("HKEY_CURRENT_USER\\Software\\Valve\\Steam", "SteamPath", null);
                if (String.IsNullOrEmpty(dirSteam) == false)
                {
                    string[] files = Directory.GetFiles(dirSteam, "BH6.exe", SearchOption.AllDirectories);
                    if (files.Length != 0)
                    {
                        foreach (String fileName in files)
                        {
                            if (fileName.Contains("\\Resident Evil 6\\"))
                                exeLocation = fileName;
                        }
                    }
                }
                if (String.IsNullOrEmpty(exeLocation))
                {
                    OpenFileDialog re6EXEFileDialog = new OpenFileDialog();
                    re6EXEFileDialog.InitialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"; //This PC GUID
                    re6EXEFileDialog.Filter = "RE6 EXE File|BH6.exe";
                    re6EXEFileDialog.RestoreDirectory = true;
                    if (re6EXEFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        exeLocation = re6EXEFileDialog.FileName;
                    }
                    else
                    {
                        MessageBox.Show("Invalid file selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            byte[] re6EXEBytes = File.ReadAllBytes(exeLocation);
            byte[] signature = { 0x7C, 0x11, 0x8B, 0xCB, 0xE8 };
            int offset = SearchBytes(re6EXEBytes, signature);
            if (offset != -1)
            {
                re6EXEBytes[offset] = 0xEB; //Replace 0x7C (Jump Lower) with 0xEB (JuMP)
                try
                {
                    File.WriteAllBytes(exeLocation, re6EXEBytes);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Failed to write BH6.exe. C# exception: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                MessageBox.Show("Successfully patched " + exeLocation + " with the vertical Lightning Hawk fix.");
            }
            else
            {
                MessageBox.Show("Failed to patch " + exeLocation + ". Maybe it's already patched?", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
